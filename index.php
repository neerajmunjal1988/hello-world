<?php echo time();?>
<script type="text/javascript">
setInterval(function(){
    // set whatever future date / time you want here, together with
    // your timezone setting...
    var future = new Date("Sep 20 2014 21:15:00 GMT+0200");
    var now = new Date();
    var difference = Math.floor((future - now) / 1000);

    var seconds = fixIntegers(difference % 60);
    difference = Math.floor(difference / 60);

    var minutes = fixIntegers(difference % 60);
    difference = Math.floor(difference / 60);

    var hours = fixIntegers(difference % 24);
    difference = Math.floor(difference / 24);

    var days = difference;

    $("#seconds").text(seconds + "s");
    $("#minutes").text(minutes + "m");
    $("#hours").text(hours + "h");
    $("#days").text(days + "d");
}, 1000);

function fixIntegers(integer)
{
    if (integer < 0)
        integer = 0;
    if (integer < 10)
        return "0" + integer;
    return "" + integer;
}
setInterval();</script>

<body><span id="days"></span>
<span id="hours"></span>
<span id="minutes"></span>
<span id="seconds"></span></body>
